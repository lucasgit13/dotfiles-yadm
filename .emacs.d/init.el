(setq inhibit-startup-message t)
(menu-bar-mode -1)
(customize-set-variable 'scroll-bar-mode nil) ;; remove scrollbars in all frames, needed for emacsclient
(tool-bar-mode -1)
(column-number-mode 1)
(global-auto-revert-mode 1)
;; (icomplete-vertical-mode 1)
(setq mode-require-final-newline nil)

(fset 'yes-or-no-p 'y-or-n-p)
(setq tab-width 4)
(setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
(setq indent-tabs-mode nil)

(add-hook 'dired-mode-hook 'auto-revert-mode)
(setq global-auto-revert-non-file-buffers t)
(setq dired-kill-when-opening-new-dired-buffer t)

(global-display-line-numbers-mode 1)

(custom-set-variables
 '(display-line-numbers-type (quote relative)))

(dolist (mode '(term-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda() (display-line-numbers-mode 0))))

(set-face-attribute 'default nil :font "Hack NF-12" )
(blink-cursor-mode 0)

(setq history-length 200)
(savehist-mode 1)
(save-place-mode 1)
(setq custom-file (locate-user-emacs-file "custom_vars.el"))
(load custom-file 'noerror 'nomessage)

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(setq use-package-always-ensure t)

(add-to-list 'load-path
             "~/.emacs.d/plugins/yasnippet")
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(setq default-frame-alist '((cursor-color . "magenta")))

(add-hook 'prog-mode-hook 'electric-pair-mode)

(add-hook 'c-mode-hook (lambda () (interactive) (c-set-style "stroustrup")))

(use-package recentf
  :config
  (recentf-mode 1)
  (setq recentf-auto-cleanup 'never
	recentf-max-saved-items 200))

(use-package yasnippet
  :hook ((c-mode . yas-minor-mode)))

(use-package company
  :hook ((prog-mode . company-mode)))

(use-package diminish)

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(use-package ivy
  :diminish
  :ensure t
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-j" . ivy-next-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :after counsel
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . 'counsel-M-x)
	 ("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1)
  (setq ivy-initial-inputs-alist nil))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))


(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  ;; Uncomment the following line to have sorting remembered across sessions!
  ;(prescient-persist-mode 1)
  (ivy-prescient-mode 1))

;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(use-package general
  :after evil
  :config
  (general-evil-setup)
  (general-create-definer ls/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (ls/leader-keys
    "SPC" '(lambda () (interactive) (counsel-M-x)))

  (ls/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "tt" '(counsel-load-theme :which-key "choose theme")
    "." '(counsel-find-file :which-key "find file")
    "fr" '(counsel-recentf :which-key "recentf")
    "fde" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/Emacs.org"))))

  (ls/leader-keys
    "w" '(:ignore t :which-key "window")
    "wo" '(delete-other-windows :which-key "delete other")
    "wc" '(evil-window-delete :which-key "delete")
    "wj" '(evil-window-down :which-key "down")
    "wk" '(evil-window-up :which-key "up")
    "wh" '(evil-window-right :which-key "right")
    "wl" '(evil-window-left :which-key "left")
    )

    ;; (general-nmap
    ;;   :keymaps 'override-global-map
    ;;   "<A-j>" ":m .1<CR>=="
    ;;   "<A-k>" ":m .-2<CR>==")


    ;;   (general-vmap
    ;; "<C-j>" ":m '>+1<CR>gv=gv"
    ;; "<C-k>" ":m '<-2<CR>gv=gv")

   (general-nmap
      :prefix "SPC"
      "v" "vg_")

    (general-vmap
    :prefix "SPC"
    "d" "\"_d")
    )

(general-setq auto-revert-interval 10
              evil-want-Y-yank-to-eol t
              evil-search-module 'evil-search)

(general-define-key "<escape>" 'keyboard-escape-quit)
(general-define-key
 "C-c p"  '(lambda () (interactive) (counsel-dired "/home/lucas/Projects/")))

(general-define-key
 :states 'motion
 "C-\\" 'vterm-toggle
 "[d" 'flymake-goto-next-error
 "]d" 'flymake-goto-prev-error
 "C-<left>" '(lambda () (interactive) (evil-window-increase-width 1))
 "C-<right>" '(lambda () (interactive) (evil-window-decrease-width 1))
 "C-<up>" '(lambda () (interactive) (evil-window-increase-height 1))
 "C-<down>" '(lambda () (interactive) (evil-window-decrease-height 1)))

(use-package evil
 :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump t)
 :config
 (evil-mode 1)
 (add-to-list 'evil-emacs-state-modes 'eshell-mode)
 (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join))

 (add-hook 'dired-mode-hook 'evil-emacs-state)
 (add-hook 'eshell-mode-hook 'evil-emacs-state)
;; (setq evil-insert-state-cursor '((bar . 10) "pink")
;;       evil-normal-state-cursor '((bar . 10) "magenta"))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(define-key evil-insert-state-map "j" #'cofi/maybe-exit)
(evil-define-command cofi/maybe-exit ()
  :repeat change
  (interactive)
  (let ((modified (buffer-modified-p)))
    (insert "j")
    (let ((evt (read-event (format "Insert %c to exit insert state" ?k)
               nil 0.5)))
      (cond
       ((null evt) (message ""))
       ((and (integerp evt) (char-equal evt ?k))
    (delete-char -1)
    (set-buffer-modified-p modified)
    (push 'escape unread-command-events))
       (t (setq unread-command-events (append unread-command-events
                          (list evt))))))))
(use-package doom-themes
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t)
  (load-theme 'doom-one t)) ; if nil, italics is universally disabled

;; doom-tomorrow-night
;; doom-material-dark
;; doom-badger
;; doom-lantern
;; doom-miramare
;; doom-monokai-pro
;; doom-oceanic-next
;; doom-octagon
;; doom-pine

;; (use-package zenburn-theme)
;; (load-theme 'zenburn t)

;; (use-package gruber-darker-theme)
;; (load-theme 'gruber-darker t)

;; (use-package marginalia
;;   :config
;;   (marginalia-mode))

;; (use-package embark
;;   :bind
;;   (("C-{ " . embark-act)         ;; pick some comfortable binding
;;    ("C-}" . embark-dwim)        ;; good alternative: M-.
;;    ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
;;   :init
;;   ;; Optionally replace the key help with a completing-read interface
;;   (setq prefix-help-command #'embark-prefix-help-command)
;;   ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
;;   ;; strategy, if you want to see the documentation from multiple providers.
;;   (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
;;   ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
;;   :config
;;   ;; Hide the mode line of the Embark live/completions buffers
;;   (add-to-list 'display-buffer-alist
;;                '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
;;                  nil
;;                  (window-parameters (mode-line-format . none)))))

;; ;; Consult users will also want the embark-consult package.
;; (use-package embark-consult
;;   :hook
;;   (embark-collect-mode . consult-preview-at-point-mode))

(use-package magit
  :commands magit-status
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(defun ls/org-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(add-hook 'org-mode-hook (lambda () (buffer-face-mode -1)))

(use-package org
  :init
  (setq org-hide-emphasis-markers t)
  :hook (org-mode . ls/org-setup)
  :config
  )
  
(use-package vterm
  :commands vterm-mode vterm-toggle
  :bind (("C-\\" . 'vterm-toggle))
  ;; :hook (vterm-mode . turn-off-evil-mode)
  )

(use-package move-text
  ;; :bind (("M-<up>" . move-text-up)
  ;; 	 ("M-<down>" . move-text-down))
  :config
  (move-text-default-bindings))