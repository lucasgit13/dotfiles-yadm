call plug#begin()
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Plug 'jayli/vim-easycomplete'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'gruvbox-community/gruvbox'
" Plug 'vim-python/python-syntax'
Plug 'nvim-lua/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'SirVer/ultisnips'
Plug 'jiangmiao/auto-pairs'

call plug#end()

set nocompatible
filetype on
syntax on
set signcolumn=yes
set exrc
set number relativenumber
set wildmenu
set incsearch
set nobackup
set noswapfile
set nowritebackup
set expandtab
set smarttab
set autoindent
set cindent
set autoread
set tabstop=4
set shiftwidth=4
set softtabstop=4
set path+=**
set t_Co=256
set termguicolors
set clipboard+=unnamedplus
set mousemodel=extend
set scrolloff=10
set mouse=a
set hidden
set termguicolors
set guicursor=
set undofile
set splitbelow splitright
set cmdheight=2
set updatetime=300
set shortmess+=c

colorscheme gruvbox
hi Normal guibg=NONE ctermbg=NONE

function StripTrailingWhitespace()
	if !&binary && &filetype != 'diff'
		normal mz
		normal Hmy
		%s/\s\+$//e
		normal 'yz<CR>
		normal `z
	endif
endfunction

let mapleader=" "
nnoremap  <SPACE> <Nop>
nnoremap <ESC> :noh<CR>
inoremap jk <Esc>
nnoremap <F2> :%s/\<<C-r><C-w>\>//g<Left><Left>
map <F5> :call StripTrailingWhitespace()<CR> :w<CR>
map <F10> :wq<CR>

" Buffer movement
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>wo :wincmd o<CR>
nnoremap <leader>wc :wincmd c<CR>
nnoremap <Leader>wk :bw!<CR>
nnoremap <leader><CR> :so $MYVIMRC<CR>
nnoremap <leader>ec :e $MYVIMRC<CR>
nnoremap <leader>eo :e ~/.config/nvim/old_init.vim<CR>

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Move lines
noremap <A-j> :m .1<CR>==
noremap <A-k> :m .-2<CR>==
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Genaral remaps
nnoremap Y yg_
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap <Leader>v vg_

" Undo break points
inoremap , ,<C-g>u
inoremap . .<C-g>u
inoremap [ [<C-g>u
inoremap ! !<C-g>u
inoremap ? ?<C-g>u

" Do not supend
nnoremap <c-z> <nop>

" Terminal
nnoremap <Leader>ty :lcd %:p:h<CR>:11new term://zsh<CR>i
" Bind 'ESC' to go to normal mode on terminal
tnoremap <Esc> <C-\><C-n>
tnoremap <C-K> <C-\><C-N><C-w>k
nnoremap <C-J> <C-w>ji

"""""""""""""""""""""""""""""""""""""
" Telescope
"""""""""""""""""""""""""""""""""""""
nnoremap <leader>tt <cmd>Telescope<cr>
nnoremap <leader>. <cmd>Telescope find_files<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>tg :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
nnoremap <C-s> <cmd>Telescope current_buffer_fuzzy_find<CR>

"""""""""""""""""""""""""""""""""""""
" Coc
"""""""""""""""""""""""""""""""""""""
" xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)
nnoremap <leader>qf  <Plug>(coc-fix-current)

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

if exists('*complete_info')
  inoremap <silent><expr> <cr> complete_info(['selected'])['selected'] != -1 ? "\<C-y>" : "\<C-g>u\<CR>"
endif
"""""""""""""""""""""""""""""""""""""
" Autocmd
"""""""""""""""""""""""""""""""""""""
" keep last cursor position
autocmd BufReadPost * silent! normal! g`"zv
autocmd BufRead,BufEnter,BufNewFile *.s setlocal commentstring=#\ %s
autocmd! bufwritepost sxhkdrc :!pkill -USR1 -x sxhkd
autocmd BufRead sxhkdrc setlocal commentstring=#\ %s
autocmd FileType python nnoremap <silent><nowait> <space>cc  :<C-u>CocCommand python.execInTerminal<cr>

autocmd FileType html source ~/.config/nvim/custom_settings/html.vim
autocmd FileType c source ~/.config/nvim/custom_settings/c.vim
autocmd FileType javascript source ~/.config/nvim/custom_settings/js.vim
autocmd FileType java source ~/.config/nvim/custom_settings/java.vim
"""""""""""""""""""""""""""""""""""""
" UltiSnips
"""""""""""""""""""""""""""""""""""""
let g:UltiSnipsSnippetDirectories=["UltiSnips", "custom_snippets"]
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

"""""""""""""""""""""""""""""""""""""
" Treesitter
"""""""""""""""""""""""""""""""""""""
lua <<EOF
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = true
  }
}
EOF
