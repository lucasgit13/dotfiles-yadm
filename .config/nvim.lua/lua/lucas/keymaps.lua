local opts = { noremap = true, silent = true }
local keymap= vim.keymap.set

keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "


------------
-- NORMAL --
------------
-- Navigate buffers
-- keymap("n", "<S-l>", ":bnext<CR>", opts)
-- keymap("n", "<S-h>", ":bprevious<CR>", opts)
keymap("n", "<leader>h", ":wincmd h<cr>", opts)
keymap("n", "<leader>j", ":wincmd j<cr>", opts)
keymap("n", "<leader>k", ":wincmd k<cr>", opts)
keymap("n", "<leader>l", ":wincmd l<cr>", opts)
keymap("n", "<leader>wc", ":wincmd c<cr>", opts)
keymap("n", "<leader>wo", ":wincmd o<cr>", opts)
keymap("n", "<leader>wk", ":bw!<cr>", opts)
keymap("n", "<leader><cr>", ":so % | echo expand('%:t') 'was sourced!'<cr>", {})
keymap("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

keymap("n", "<F5>", ":w<CR>", opts)
keymap("n", "<F10>", ":wq<CR>", opts)

keymap("n", "Y", "yg_", opts)
keymap("n", "<leader>v", "vg_", opts)

-- Yank to system clipboard
keymap({"n", "v"}, "<C-y>", "\"+y", opts)

-- Paste from system clipboard
keymap({"n", "v"}, "<C-p>", "\"+p==", opts)

-- Move text up and down
keymap("n", "<A-j>", ":m .1<CR>==", opts)
keymap("n", "<A-k>", ":m .-2<CR>==", opts)

-- Explorer
keymap("n", "<Leader>x", ":Lex 20<CR>", opts)

-- Do not sleep
-- keymap("n", "<C-z>", "<nop>", opts)
keymap("n", "Q", "<nop>")
keymap("n", "<Esc>", ":noh<cr>", opts)

-- Packer
keymap("n", "<leader>ps", ":PackerSync<cr>", opts)

------------
-- INSERT --
------------
keymap({"v", "i" }, "jk", "<ESC>", opts)


------------
-- VISUAL --
------------
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<A-k>", ":m '<-2<CR>gv=gv", opts)

keymap("v", "p", '"_dP', opts)
keymap("v", "<leader>d", '"_d', opts)


---------------
-- Telescope --
---------------
local builtin = require('telescope.builtin')
-- keymap("n", "<leader>.", "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_ivy())<cr>", opts)
keymap("n", "<leader>.", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", opts)
keymap("n", "<leader>fb", "<cmd>Telescope buffers<cr>", opts)
keymap("n", "<leader>fr", "<cmd>Telescope oldfiles<cr>", opts)
keymap("n", "<leader>tt", "<cmd>Telescope<cr>", opts)
keymap("n", "<C-s>", "<cmd>Telescope current_buffer_fuzzy_find<CR>", opts)
-- keymap("n", "<C-p>", "<cmd>Telescope git_files<CR>", opts)
keymap("n", "<leader>ff", builtin.live_grep, opts)
keymap("n", "<leader>gf", builtin.git_files, opts)
keymap("n", "<leader>gs", builtin.grep_string, opts)

