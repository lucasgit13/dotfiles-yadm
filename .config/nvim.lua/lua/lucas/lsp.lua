local lsp = require('lsp-zero').preset({})
--keymap = vim.lsp.keymap.set

lsp.on_attach(function(client, bufnr)
    local opts = { buffer = bufnr, remap = false }
    -- lsp.default_keymaps({ buffer = bufnr })
    vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, opts)
    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "go", function() vim.lsp.buf.type_definition() end, opts)
    vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, opts)
    vim.keymap.set("n", "gs", function() vim.lsp.buf.signature_help() end, opts)
    vim.keymap.set("n", "<F2>", function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set("n", "<leader>la", function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>lf", function() vim.lsp.buf.format() end, opts)
    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set("n", "gl", function() vim.diagnostic.open_float() end, opts)
end)

require('lspconfig').eslint.setup({
    single_file_support = true,
    on_attach = function(client, bufnr)
        print('hello eslint')
    end
})

-- When you don't have mason.nvim installed
-- You'll need to list the servers installed in your system
lsp.setup_servers({'clang', 'tsserver', 'pyright', 'lua_ls' })

-- lsp.format_mapping('gq', {
--     format_opts = {
--         async = false,
--         timeout_ms = 10000,
--     },
--     servers = {
--         ['null-ls'] = { 'javascript', 'typescript', 'lua', 'python' },
--     }
-- })

-- Servers settings
lsp.configure('lua_ls', {
    settings = {
        Lua = {
            diagnostics = {
                globals = { "vim" }
            },
            workspace = {
                library = {
                    [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                    [vim.fn.stdpath("config") .. "/lua"] = true,
                },
            },
        },
    },
})

lsp.configure('tsserver', {
  single_file_support = false,
  on_attach = function(client, bufnr)
  end
})

lsp.setup()


-- Cmp
local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

-- require('luasnip.loaders.from_vscode').lazy_load()
require('luasnip.loaders.from_snipmate').lazy_load()

cmp.setup({
    sources = {
        { name = 'path' },
        { name = 'nvim_lsp' },
        { name = 'buffer',  keyword_length = 3 },
        { name = 'luasnip', keyword_length = 2 },
    },
    mapping = {
        ['<CR>'] = cmp.mapping.confirm({select = false}),
        ['<Tab>'] = cmp_action.tab_complete(),
        ['<S-Tab>'] = cmp_action.select_prev_or_fallback(),
        ['<C-j>'] = cmp_action.luasnip_jump_forward(),
        ['<C-k>'] = cmp_action.luasnip_jump_backward(),
    }
})


-- Null-ls
local null_ls = require('null-ls')

null_ls.setup({
    sources = {
        -- Replace these with the tools you have installed
        -- make sure the source name is supported by null-ls
        -- https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md
        null_ls.builtins.formatting.prettier,
        -- null_ls.builtins.diagnostics.eslint,
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.formatting.black,
        null_ls.builtins.formatting.clang_format,
        -- null_ls.builtins.formatting.deno_fmt, -- will use the source for all supported file types
        -- null_ls.builtins.formatting.deno_fmt.with({
        --     filetypes = { "markdown" },       -- only runs `deno fmt` for markdown
        -- }),
    }
})
