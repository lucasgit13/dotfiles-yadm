local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

return packer.startup(function(use)

    use 'wbthomason/packer.nvim'
    use "akinsho/toggleterm.nvim"
    use "windwp/nvim-autopairs"
    use { 'nvim-lualine/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', opt = true } }
    use { "nvim-telescope/telescope.nvim", requires = { "nvim-lua/plenary.nvim" } }
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end,
    }

    -- Colorscheme
    -- use { 'folke/tokyonight.nvim' }
    -- use { 'rktjmp/lush.nvim' }
    use { "ellisonleao/gruvbox.nvim" }
    -- use { 'sainnhe/gruvbox-material' }
    -- use({ 'rose-pine/neovim', as = 'rose-pine' })
    -- use "savq/melange-nvim"
    -- use "rebelot/kanagawa.nvim"
    use 'Mofiqul/vscode.nvim'
    use "hsanson/vim-android"

    -- Cmp
    -- use 'hrsh7th/cmp-nvim-lsp'
    -- use 'hrsh7th/cmp-buffer'
    -- use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    -- use 'hrsh7th/nvim-cmp'

    -- Ultisnips
    -- use 'SirVer/ultisnips'
    -- use 'quangnguyen30192/cmp-nvim-ultisnips'
    use 'honza/vim-snippets'

    -- Tpope
    use 'tpope/vim-commentary'
    use 'tpope/vim-surround'
    use 'tpope/vim-repeat'

    -- LSP
    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
        requires = {
            -- LSP Support
            {'neovim/nvim-lspconfig'},             -- Required
            {                                      -- Optional
            'williamboman/mason.nvim',
            run = function()
                pcall(vim.cmd, 'MasonUpdate')
            end,
        },
        {'williamboman/mason-lspconfig.nvim'}, -- Optional

        -- Autocompletion
        {'hrsh7th/nvim-cmp'},     -- Required
        {'hrsh7th/cmp-nvim-lsp'}, -- Required
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-path',
        {'saadparwaiz1/cmp_luasnip'},
        {'L3MON4D3/LuaSnip'},     -- Required
        "rafamadriz/friendly-snippets",
        {'jose-elias-alvarez/null-ls.nvim'} -- Formatter
        }
    }

    -- Vimwiki
    use "vimwiki/vimwiki"

    -- Prettier
    use('MunifTanjim/prettier.nvim')

    if PACKER_BOOTSTRAP then
        require("packer").sync()
    end
end)
