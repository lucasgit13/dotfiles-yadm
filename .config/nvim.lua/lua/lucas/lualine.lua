local status_ok, lualine = pcall(require, "lualine")
if not status_ok then
	return
end

local hide_in_width = function()
	return vim.fn.winwidth(0) > 80
end

local diagnostics = {
	"diagnostics",
	sources = { "nvim_diagnostic" },
	sections = { "error", "warn" },
	symbols = { error = " ", warn = " " },
	colored = false,
	update_in_insert = false,
	always_visible = true,
}

local diff = {
    "diff",
    colored = false,
    symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
    cond = hide_in_width
}

local mode = {
	"mode",
    icons_enabled = true,
}

-- local buffer = {
--     'buffers',
--     mode = 0,
--     show_modified_status = true,
--     symbols = {
--         modified = ' ●',      -- Text to show when the buffer is modified
--         alternate_file = '#', -- Text to show to identify the alternate file
--         directory =  '',     -- Text to show when the buffer is a directory
--   }
-- }

local filename = {
      'filename',
      file_status = true,      -- Displays file status (readonly status, modified status)
      path = 3,                -- 0: Just the filename
                               -- 1: Relative path
                               -- 2: Absolute path
                               -- 3: Absolute path, with tilde as the home directory

      shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                               -- for other components. (terrible name, any suggestions?)
      symbols = {
        modified = '[+]',      -- Text to show when the file is modified.
        readonly = '[-]',      -- Text to show when the file is non-modifiable or readonly.
        unnamed = '[No Name]', -- Text to show for unnamed buffers.
      }
}

local filetype = {
	"filetype",
	icons_enabled = true,
	icon = true,
}

local branch = {
	"branch",
	icons_enabled = true,
	icon = "",
}

local location = {
	"location",
	padding = 0,
}

local fileFormat = {
    'fileformat',
        symbols = {
        unix = '', -- e712
        dos = '',  -- e70f
        mac = '',  -- e711
    },
}

lualine.setup({
	options = {
		icons_enabled = true,
		theme = "seoul256",
		component_separators = { left = "", right = "" },
		section_separators = { left = "", right = "" },
		disabled_filetypes = { "alpha", "dashboard", "NvimTree", "Outline" },
		always_divide_middle = true,
	},
	sections = {
        lualine_a = { mode },
        lualine_b = { filename },
		lualine_c = {},
		-- lualine_x = { "encoding", "fileformat", "filetype" },
		lualine_x = { diff, fileFormat, "encoding", filetype },
		lualine_y = { location },
        lualine_z = { branch, diagnostics },
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { "filename" },
		lualine_x = { "location" },
		lualine_y = {},
        lualine_z = {},
	},
	tabline = {},
	extensions = {},
})

