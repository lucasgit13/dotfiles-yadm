function ColorScheme(color)
    color = color or "vscode" 
    vim.cmd.colorscheme(color)
    -- vim.api.nvim_set_hl(0, "Normal", {bg = "none"})
    -- vim.api.nvim_set_hl(0, "NormalFloat", {bg = "none"})
end

ColorScheme()

-- vim.cmd [[
-- try
--     colorscheme gruvbox
-- catch /^Vim\%((\a\+)\)\=:E185/
--     colorscheme default
--     set background=dark
-- endtry
-- ]]
-- vim.o.background = 'dark'
