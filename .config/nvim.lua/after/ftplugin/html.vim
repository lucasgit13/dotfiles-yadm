inoremap ;p <p></p><Esc>F<i
inoremap ;n <nav></nav><Esc>F<i
inoremap ;ht <html></html><Esc>F<i
inoremap ;bo <body></body><Esc>F<i

inoremap ;h1 <h1></h1><Esc>F<i
inoremap ;h2 <h2></h2><Esc>F<i
inoremap ;h3 <h3></h3><Esc>F<i
inoremap ;h4 <h4></h4><Esc>F<i
inoremap ;h5 <h5></h5><Esc>F<i
inoremap ;h6 <h6></h6><Esc>F<i
