let g:gradle_path = "/home/lucas/Android/Sdk/"
autocmd BufReadPost * silent! normal! g`"zv
autocmd BufRead,BufEnter,BufNewFile *.s setlocal commentstring=#\ %s
autocmd BufRead sxhkdrc setlocal commentstring=#\ %s
" autocmd BufRead,BufEnter,BufNewFile *.js,*.jsx,*.py TSBufEnable highlight
autocmd! bufwritepost sxhkdrc :!pkill -USR1 -x sxhkd
autocmd BufRead vifmrc setfiletype vim

autocmd FileType javascript,javascriptreact silent! source ~/.config/nvim/after/ftplugin/html.vim
autocmd FileType javascriptreact silent! source ~/.config/nvim/after/ftplugin/javascript.vim
autocmd FileType c,cpp silent! source ~/.config/nvim/after/ftplugin/decl_func.vim
autocmd FileType dart silent! source ~/.config/nvim/after/dart.vim
autocmd FileType java silent! source ~/.config/nvim/after/java.vim

" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<c-j>"
" let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:python3_host_prog="/usr/bin/python"

function StripTrailingWhitespace()
    if !&binary && &filetype != 'diff'
        normal mz
        normal Hmy
        %s/\s\+$//e
        normal 'yz<CR>
        normal `z
        endif
        endfunction

autocmd! bufwritepost  * call StripTrailingWhitespace()

" augroup StripTrailingWhitespace
"     autocmd!
"     autocmd BufWritePre * %s/\s\+$//e
" augroup END

" Undo break points
inoremap , ,<C-g>u
inoremap . .<C-g>u
inoremap [ [<C-g>u
inoremap ! !<C-g>u
inoremap ? ?<C-g>u


nnoremap <ESC> :noh<CR>
