require "lucas.autopairs"
-- require "lucas.cmp"
require "lucas.keymaps"
require "lucas.lsp"
require "lucas.lualine"
require "lucas.options"
require "lucas.plugins"
require "lucas.toggleterm"
require "lucas.telescope"
require "lucas.colorscheme"
--require "lucas.prettier"
require "lucas.treesitter"
--require "lucas.autocmd"
