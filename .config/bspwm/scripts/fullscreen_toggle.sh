#!/usr/bin/env bash
#
set -euo pipefail

file="$HOME/.config/bspwm/scripts/bspwm_states"

layout=$(grep "current_layout" "${file}" | cut -d' ' -f2)

if [ $layout = "tiled" ]; then
    bspc node -t fullscreen
    sed -s 's-tiled-fullscreen-' "${file}" -i

elif [ $layout = "fullscreen" ]; then
    bspc node -t tiled
    sed -s 's-fullscreen-tiled-' "${file}" -i
fi
